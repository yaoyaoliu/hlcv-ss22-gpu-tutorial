#!/usr/bin/env python
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import numpy as np
import PIL
import copy

# for reproducibility
# Ref: https://pytorch.org/docs/stable/notes/randomness.html
np.random.seed(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
torch.manual_seed(0)


def weights_init(m):
    if type(m) == nn.Linear:
        nn.init.normal_(m.weight.data, mean=0.0, std=1e-3)
        m.bias.data.fill_(0.0)

    if isinstance(m, nn.Conv2d):
        nn.init.xavier_normal_(m.weight.data)
        m.bias.data.fill_(0.0)

def update_lr(optimizer, lr):
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

#-------------------------------------------------
# Calculate the model size (Q1.b)
# if disp is true, print the model parameters, otherwise, only return the number of parameters.
#-------------------------------------------------
def PrintModelSize(model, disp=True):
    #################################################################################
    # TODO: Implement the function to count the number of trainable parameters in   #
    # the input model. This useful to track the capacity of the model you are       #
    # training                                                                      #
    #################################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('Total number of trainable parameters: {}'.format(pytorch_total_params))
    model_sz = pytorch_total_params
    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
    return model_sz

# The function to show transformed data
def show_dataset(dataset, n=6):
  #img = np.vstack((np.hstack((np.asarray(dataset[i][0]) for _ in range(n))) for i in range(n)))
  img = np.vstack(
      (np.hstack((np.asarray(dataset[i][0]) for _ in range(n))) for i in range(n))
  )
  plt.imshow(img)
  plt.axis('off')
#--------------------------------
# Device configuration
#--------------------------------
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('Using device: %s'%device)

#--------------------------------
# Hyper-parameters
#--------------------------------
input_size = 3
num_classes = 10
num_training= 49000
num_validation =1000
batch_size = 128

norm_layer = None


#-------------------------------------------------
# Load the CIFAR-10 dataset
#-------------------------------------------------
#################################################################################
# TODO: Q3.a Chose the right data augmentation transforms with the right        #
# hyper-parameters and put them in the data_aug_transforms variable             #
#################################################################################
data_aug_transforms = []
# *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
"""data_aug_transforms = [
    transforms.ColorJitter(brightness=.01, contrast=.01, saturation=.01, hue=.01),
    transforms.RandomAffine(5, resample=PIL.Image.BILINEAR),
    transforms.RandomHorizontalFlip(),
    transforms.Resize((32,32))
]"""
# *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
norm_transform = transforms.Compose(data_aug_transforms+[transforms.ToTensor(),
                                     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                                     ])
test_transform = transforms.Compose([transforms.ToTensor(),
                                     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                                     ])
cifar_dataset = torchvision.datasets.CIFAR10(root='datasets/',
                                           train=True,
                                           transform=norm_transform,
                                           download=True)

test_dataset = torchvision.datasets.CIFAR10(root='datasets/',
                                          train=False,
                                          transform=test_transform,
                                          download=True
                                          )
#-------------------------------------------------
# Prepare the training and validation splits
#-------------------------------------------------
mask = list(range(num_training))
train_dataset = torch.utils.data.Subset(cifar_dataset, mask)
mask = list(range(num_training, num_training + num_validation))
val_dataset = torch.utils.data.Subset(cifar_dataset, mask)

#-------------------------------------------------
# Data loader
#-------------------------------------------------
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)

val_loader = torch.utils.data.DataLoader(dataset=val_dataset,
                                           batch_size=batch_size,
                                           shuffle=False)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)


#-------------------------------------------------
# Convolutional neural network (Q1.a and Q2.a)
# Set norm_layer for different networks whether using batch normalization
#-------------------------------------------------
hidden_size = [32,  64,  64,  64,  64,  64]
kernel_size = 3

class ConvNet(nn.Module):
    def __init__(self, input_size, hidden_layers, num_classes, norm_layer=0.1,
                 use_dropout=False, dropout_prob=0.0, use_batchnorm=False):
        super(ConvNet, self).__init__()
        #################################################################################
        # TODO: Initialize the modules required to implement the convolutional layer    #
        # described in the exercise.                                                    #
        # For Q1.a make use of conv2d and relu layers from the torch.nn module.         #
        # For Q2.a make use of BatchNorm2d layer from the torch.nn module.              #
        # For Q3.b Use Dropout layer from the torch.nn module.                          #
        #################################################################################
        layers = []
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        self.conv1 = nn.Conv2d(input_size, hidden_layers[0], kernel_size, stride=1, padding=1)
        self.conv2 = nn.Conv2d(hidden_layers[0], hidden_layers[1], kernel_size, stride=1, padding=1)
        self.conv3 = nn.Conv2d(hidden_layers[1], hidden_layers[2], kernel_size, stride=1, padding=1)
        self.conv4 = nn.Conv2d(hidden_layers[2], hidden_layers[3], kernel_size, stride=1, padding=1)
        self.conv5 = nn.Conv2d(hidden_layers[3], hidden_layers[4], kernel_size, stride=1, padding=1)

        self.bn1 = nn.BatchNorm2d(hidden_layers[0], momentum=norm_layer)
        self.bn2 = nn.BatchNorm2d(hidden_layers[1], momentum=norm_layer)
        self.bn3 = nn.BatchNorm2d(hidden_layers[2], momentum=norm_layer)
        self.bn4 = nn.BatchNorm2d(hidden_layers[3], momentum=norm_layer)
        self.bn5 = nn.BatchNorm2d(hidden_layers[4], momentum=norm_layer)


        self.fc1 = nn.Linear(hidden_layers[4], hidden_layers[5])
        self.fc2 = nn.Linear(hidden_layers[5], num_classes)

        self.use_dropout = use_dropout
        self.dropout_prob = dropout_prob
        self.use_batchnorm = use_batchnorm
        self.momentum = norm_layer

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

    def forward(self, x):
        #################################################################################
        # TODO: Implement the forward pass computations                                 #
        #################################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        x = self.conv1(x)
        x = F.dropout(x, p=self.dropout_prob, training=self.training) if self.use_dropout else x
        x = self.bn1(x) if self.use_batchnorm else x
        x = F.max_pool2d(x, 2)
        x = F.relu(x)

        x = self.conv2(x)
        x = F.dropout(x, p=self.dropout_prob, training=self.training) if self.use_dropout else x
        x = self.bn2(x) if self.use_batchnorm else x
        x = F.max_pool2d(x, 2)
        x = F.relu(x)

        x = self.conv3(x)
        x = F.dropout(x, p=self.dropout_prob, training=self.training) if self.use_dropout else x
        x = self.bn3(x) if self.use_batchnorm else x
        x = F.max_pool2d(x, 2)
        x = F.relu(x)

        x = self.conv4(x)
        x = F.dropout(x, p=self.dropout_prob, training=self.training) if self.use_dropout else x
        x = self.bn4(x) if self.use_batchnorm else x
        x = F.max_pool2d(x, 2)
        x = F.relu(x)

        x = self.conv5(x)
        x = F.dropout(x, p=self.dropout_prob, training=self.training) if self.use_dropout else x
        x = self.bn5(x) if self.use_batchnorm else x
        x = F.max_pool2d(x, 2)
        x = F.relu(x)

        # to FC layers
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        return x
        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        #return out


#======================================================================================
# Q1.a: Implementing convolutional neural net in PyTorch
#======================================================================================
# In this question we will implement a convolutional neural networks using the PyTorch
# library.  Please complete the code for the ConvNet class evaluating the model
#--------------------------------------------------------------------------------------
dropout_conf = (False, 0.3)
batchnorm_conf = (True, 0.1)
model = ConvNet(input_size, hidden_size, num_classes,
                norm_layer=batchnorm_conf[1], use_batchnorm=batchnorm_conf[0],
                use_dropout=dropout_conf[0], dropout_prob=dropout_conf[1]
                ).to(device)
# Q2.a - Initialize the model with correct batch norm layer

model.apply(weights_init)
# Print the model
print(model)
# Print model size
#======================================================================================
# Q1.b: Implementing the function to count the number of trainable parameters in the model
#======================================================================================
PrintModelSize(model)
#======================================================================================
# Q1.a: Implementing the function to visualize the filters in the first conv layers.
# Visualize the filters before training
#======================================================================================

# Loss and optimizer
learning_rate = 2e-3
learning_rate_decay = 0.95
reg = 5e-4
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=reg)

# Train the model
lr = learning_rate
total_step = len(train_loader)
num_epochs = 30

# for early stopping and plotting
best_acc = 0.0
loss_store = []
val_acc_store =[]
counter = 0
max_epochs_stop = 3
epochs_no_improve = 0
best_model_wts = None

for epoch in range(num_epochs):
    running_loss = 0.0
    for i, (images, labels) in enumerate(train_loader):
        # Move tensors to the configured device
        images = images.to(device)
        labels = labels.to(device)

        # Forward pass
        outputs = model(images)
        loss = criterion(outputs, labels)

        counter += 1
        running_loss += loss.item()

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if (i+1) % 100 == 0:
            print ('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'
                   .format(epoch+1, num_epochs, i+1, total_step, loss.item()))
            loss_store.append([counter, running_loss / 100])
            running_loss = 0.0

    # Code to update the lr
    lr *= learning_rate_decay
    update_lr(optimizer, lr)
    model.eval()
    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in val_loader:
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

        print('Validataion accuracy is: {} %'.format(100 * correct / total))
        val_acc_store.append([epoch+1, (100 * correct / total)])
        #################################################################################
        # TODO: Q2.b Implement the early stopping mechanism to save the model which has #
        # acheieved the best validation accuracy so-far.                                #
        #################################################################################
        best_model = None
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        # Track the best model to save it and do early stopping
        acc =  100 * correct / total
        if acc >= best_acc:
            best_acc = acc
            best_model_wts = copy.deepcopy(model.state_dict())
            epochs_no_improve = 0
        else:
            epochs_no_improve += 1
            if epochs_no_improve >= max_epochs_stop:
              print('Early Stopping Mechanism')
              break
        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    model.train()

# Test the model
# In test phase, we don't need to compute gradients (for memory efficiency)
model.eval()
#################################################################################
# TODO: Q2.b Implement the early stopping mechanism to load the weights from the#
# best model so far and perform testing with this model.                        #
#################################################################################
# *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
model.load_state_dict(best_model_wts)
# *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in test_loader:
        images = images.to(device)
        labels = labels.to(device)
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()
        if total == 1000:
            break

    print('Accuracy of the network on the {} test images: {} %'.format(total, 100 * correct / total))

# Q1.c: Implementing the function to visualize the filters in the first conv layers.
# Save the model checkpoint
torch.save(model.state_dict(), 'model.ckpt')